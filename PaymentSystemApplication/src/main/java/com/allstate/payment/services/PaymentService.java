package com.allstate.payment.services;

import com.allstate.payment.dto.Payment;
import com.allstate.payment.exception.OutOfRangeException;

import java.util.List;

public interface PaymentService {
    int rowCount();
    Payment findById(int id) throws OutOfRangeException;
    List<Payment> findByType(String type) throws OutOfRangeException;
    int save(Payment payment) throws OutOfRangeException;
}
