package com.allstate.payment.services;

import com.allstate.payment.dao.PaymentRepo;
import com.allstate.payment.dto.Payment;
import com.allstate.payment.exception.OutOfRangeException;

import java.util.List;

public class PaymentServiceImpl implements PaymentService{
    private PaymentRepo paymentRepo;

    public PaymentServiceImpl(PaymentRepo paymentRepo){
        this.paymentRepo = paymentRepo;
    }

    @Override
    public int rowCount() {
        return paymentRepo.rowCount();
    }

    @Override
    public Payment findById(int id) throws OutOfRangeException {
        if(id<1){
            throw new OutOfRangeException("Payment id not valid");
        }
        return paymentRepo.findById(id);
    }

    @Override
    public List<Payment> findByType(String type) throws OutOfRangeException {
        if(type.equalsIgnoreCase("")){
            throw new OutOfRangeException("Address type cannot be empty");
        }
        return paymentRepo.findByType(type);
    }

    @Override
    public int save(Payment payment) throws OutOfRangeException {
        if(payment.getId()<1){
            throw new OutOfRangeException("Payment id < 1");
        }
        Payment payment1 = paymentRepo.findById(payment.getId());
        if(payment1 == null){
            return paymentRepo.save(payment);
        }
        return -1;
    }
}
