package com.allstate.payment.dto;

import java.util.Date;

public class Payment {

    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int custId;

    public Payment() {
            this(1,new Date(),"Cash",1200.00,12201);
    }

    public Payment(int id, Date paymentDate, String type, double amount, int custId) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.custId = custId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    @Override
    public String toString() {
        String str = String.format("[%d] %.2f %s %d", id, amount,type,custId);
        return str;
    }
}
