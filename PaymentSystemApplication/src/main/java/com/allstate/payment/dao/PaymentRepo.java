package com.allstate.payment.dao;

import com.allstate.payment.dto.Payment;

import java.util.List;

public interface PaymentRepo {
    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);


}
