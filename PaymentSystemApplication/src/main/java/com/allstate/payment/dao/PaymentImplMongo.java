package com.allstate.payment.dao;

import com.allstate.payment.dto.Payment;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.mongodb.client.model.Filters.eq;

public class PaymentImplMongo implements PaymentRepo{

    private MongoCollection<Document> collection;

    public PaymentImplMongo(){
        getCollection();
    }

    public void getCollection(){
        MongoClient mongoClient = new MongoClient("localhost",27017);
        MongoDatabase database = mongoClient.getDatabase("paymentdb");
        this.collection = database.getCollection("payment");
    }

    public boolean isConnected() {
       if (collection.count()>=0)
        {
            return  true;
        }
        else {
            return false;
        }
    }

    @Override
    public int rowCount() {
        AtomicInteger count = new AtomicInteger();
        collection.find().forEach((Block<Document>) myDoc -> {
            count.getAndIncrement();
        });
        return count.get();
    }

    @Override
    public Payment findById(int id) {
       Document myDoc = collection.find(eq("id",id)).first();
       Payment payment = new Payment();
       payment.setId(myDoc.getInteger("id"));
       payment.setAmount(myDoc.getDouble("amount"));
       payment.setPaymentDate(myDoc.getDate("paymentDate"));
       payment.setType(myDoc.getString("type"));
       payment.setCustId(myDoc.getInteger("custId"));
       return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        List<Payment> paymentList= new ArrayList<>();
        collection.find(eq("type",type)).forEach((Block<Document>) myDoc -> {
            Payment payment = new Payment();
            payment.setId(myDoc.getInteger("id"));
            payment.setAmount(myDoc.getDouble("amount"));
            payment.setPaymentDate(myDoc.getDate("paymentDate"));
            payment.setType(myDoc.getString("type"));
            payment.setCustId(myDoc.getInteger("custId"));
            paymentList.add(payment);
        });
        return paymentList;
    }

    @Override
    public int save(Payment payment) {
        boolean isInserted = false;
        Document insertPayment = new Document("id", payment.getId())
                 .append("paymentDate", payment.getPaymentDate())
                 .append("type", payment.getType())
                 .append("amount", payment.getAmount())
                 .append("custId", payment.getCustId());
        try {
            collection.insertOne(insertPayment);
            return 0;
        }catch(Exception e){
            System.out.println("Mongo DB Save () Exception - " + e.getMessage());
            return -1;
        }
    }

    public void clearDB(){
       collection.deleteMany(new Document());
    }
}
