package com.allstate.payment.dao;

import com.allstate.payment.dto.Payment;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.InsertOneOptions;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class PaymentDAO {

    private MongoClient mongoClient;
    private MongoDatabase database;
    private MongoCollection<Document> collection;

    public PaymentDAO() {
        mongoClient = new MongoClient("localhost",27017);
        database = mongoClient.getDatabase("paymentdb");
        collection = database.getCollection("payment");
    }

    public int rowCount(){
        FindIterable<Document> findIterable = collection.find();
        int count = 0;
        try (MongoCursor<Document> iterator = findIterable.iterator()){
            while (iterator.hasNext()) {
                iterator.next();
                count++;
            }
        }
        return count;
    }

    public Payment findById(int id){
        System.out.println("---- FindbyId ------");
        FindIterable<Document> findIterable = collection.find(eq("id", id));
        Document document = findIterable.first();
        System.out.println("Document "  + document.toJson());
        Payment payment = new Payment();
        payment.setId(document.getInteger("id"));
        payment.setAmount((double)document.getDouble("amount"));
        payment.setPaymentDate(document.getDate("paymentDate"));
        payment.setType(document.getString("type"));
        payment.setCustId((int)document.getInteger("custId"));
        System.out.println(" First Document - FindById " + payment);
        return payment;
    }

    public List<Payment> findByType(String type){
        System.out.println("----- FindByType -----");
        FindIterable<Document> findIterable = collection.find(eq("type",type));
        List<Payment> lstPayment = new ArrayList<Payment>();
        int count = 0;
        try (MongoCursor<Document> iterator = findIterable.iterator()){
            while (iterator.hasNext()) {
                Document document = iterator.next();
                Payment payment = new Payment();
                payment.setId(document.getInteger("id"));
                payment.setAmount(document.getDouble("amount"));
                payment.setPaymentDate(document.getDate("paymentDate"));
                payment.setType(document.getString("type"));
                payment.setCustId(document.getInteger("custId"));
                lstPayment.add(payment);
            }
        }
        return lstPayment;
    }

    public int save(Payment payment){
        System.out.println("----- Save Payment -----");
        boolean isInserted = false;
      try {
          Document insertPayment = new Document("id", payment.getId())
                  .append("paymentDate", payment.getPaymentDate())
                  .append("type", payment.getType())
                  .append("amount", payment.getAmount())
                  .append("custId", payment.getCustId());
          collection.insertOne(insertPayment);
          isInserted = true;
      }catch(Exception e){
          System.out.println("Mongo DB Save () Exception - " + e.getMessage());
      }
       int inserted =  isInserted?1:0;
      return inserted;
    }

    public void clearDB(){
        collection.deleteMany(new Document());
    }
    
}
