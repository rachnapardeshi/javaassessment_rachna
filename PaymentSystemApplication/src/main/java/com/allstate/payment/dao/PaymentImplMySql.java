package com.allstate.payment.dao;

import com.allstate.payment.dto.Payment;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PaymentImplMySql implements PaymentRepo{

    private Connection connection;

    public PaymentImplMySql(){
        getConnection();
    }

    private Connection getConnection(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/training?serverTimezone=UTC&useSSL=false", "root", "c0nygre");
        }catch(ClassNotFoundException | SQLException e){
            System.out.println(e.getMessage());
        }
        return connection;
    }

    public boolean isConnected(){
        Statement statement = null;
        try {
            connection = getConnection();
            statement = connection.createStatement();
            ResultSet rs=statement.executeQuery("select 1");
            return true;
        }
        catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int rowCount() {
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "select * from payment");
            ResultSet resultSet =  preparedStatement.executeQuery();
            int count =0;
            while( resultSet.next()) {
                count++;
            }
            return count;
        } catch (SQLException throwables) {
            return -1;
        }
    }

    @Override
    public Payment findById(int id) {
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "select * from payment where id=?");
            preparedStatement.setInt(1,id);
            ResultSet resultSet =  preparedStatement.executeQuery();
            resultSet.next();
            Payment payment = new Payment(
                    resultSet.getInt("id"),resultSet.getDate("paymentDate"),
                    resultSet.getString("type"),resultSet.getDouble("amount"),
                    resultSet.getInt("custId"));
            return payment;
        } catch (SQLException throwables) {
            return null;
        }
    }

    @Override
    public List<Payment> findByType(String type) {
        List<Payment> paymentList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "select * from payment where type=?");
            preparedStatement.setString(1,type);
            ResultSet resultSet =  preparedStatement.executeQuery();
            while( resultSet.next()) {
                Payment payment = new Payment(
                        resultSet.getInt("id"),resultSet.getDate("paymentDate"),
                        resultSet.getString("type"),resultSet.getDouble("amount"),
                        resultSet.getInt("custId"));
                paymentList.add(payment);
            }
            return paymentList;
        } catch (SQLException throwables) {
            return null;
        }
    }

    @Override
    public int save(Payment payment) {
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "insert into payment(paymentDate,type,amount,custId) values(?,?,?,?)");
            preparedStatement.setDate(1,new java.sql.Date(payment.getPaymentDate().getTime()));
            preparedStatement.setString(2,payment.getType());
            preparedStatement.setDouble(3,payment.getAmount());
            preparedStatement.setDouble(4,payment.getCustId());
            return preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return -1;
        }
    }

    public void clearDB(){
        try{
            PreparedStatement statement = this.connection.prepareStatement(
                    "Truncate table payment");
            statement.execute();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
}
