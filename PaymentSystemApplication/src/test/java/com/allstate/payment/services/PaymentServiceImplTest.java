package com.allstate.payment.services;

import com.allstate.payment.dao.PaymentRepo;
import com.allstate.payment.dto.Payment;
import com.allstate.payment.exception.OutOfRangeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PaymentServiceImplTest {
    @Mock
    private PaymentRepo paymentRepo;
    private PaymentService paymentService;

    @BeforeEach
    public void setup(){
        paymentService = new PaymentServiceImpl(paymentRepo);
    }

    @Test
    public void testRowCountReturnsInt(){
        Payment payment = new Payment(1,new Date(),"Cash",120.00,1221);
        when(paymentRepo.rowCount()).thenReturn(1);
        assertTrue(paymentService.rowCount() == 1);
    }

    @Test
    public void testFindByIdReturnsPayment() throws OutOfRangeException {
        Payment payment = new Payment(1,new Date(),"Cash",120.00,1221);
        when(paymentRepo.findById(1)).thenReturn(payment);
        Payment payment1 = paymentService.findById(1);
        assertTrue(payment1.getType().equals("Cash"));
    }

    @Test
    public void testFindByIdThrowsException(){
        Exception exception = assertThrows(OutOfRangeException.class
        ,()-> {Payment payment = paymentService.findById(0);
        });
    }

    @Test
    public void testFindByTypeThrowsException(){
     Exception exception = assertThrows(OutOfRangeException.class,() -> {
         List<Payment> paymentList = paymentService.findByType("");
     });
    }

    @Test
    public void testFindByTypeReturnsListOfPayment() throws OutOfRangeException {
        List<Payment> paymentList = new ArrayList<>();
        paymentList.add(new Payment(1,new Date(),"Cash",120.00,1221));
        paymentList.add(new Payment(2,new Date(),"Cash",160.00,2345));
        paymentList.add(new Payment(3,new Date(),"Cash",920.00,7009));
        when(paymentRepo.findByType("Cash")).thenReturn(paymentList);
        assertTrue(paymentService.findByType("Cash").size()==3);
    }

    @Test
    public void testSaveReturnsInt() throws OutOfRangeException {
        Payment payment = new Payment(45,new Date(),"Cash",120.00,1221);
        assertTrue(paymentService.save(payment) != -1);
    }
}
