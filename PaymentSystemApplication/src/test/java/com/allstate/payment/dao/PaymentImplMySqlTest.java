package com.allstate.payment.dao;

import com.allstate.payment.dto.Payment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaymentImplMySqlTest {
  PaymentRepo paymentRepo;

  @BeforeEach
    public void setup(){
      paymentRepo = new PaymentImplMySql();
      paymentRepo.save(new Payment(1,new Date(),"Cash",120.00,1221));
  }

  @AfterEach
  public void tearDown(){
      ((PaymentImplMySql)paymentRepo).clearDB();
  }

  @Test
  public void isConnectedTest(){
      assertTrue(((PaymentImplMySql)paymentRepo).isConnected());
  }

  @Test
  public void testRowCountReturnsInt(){
    int count = paymentRepo.rowCount();
    assertTrue(count!=-1);
  }

  @Test
  public void testFindByIdReturnsPayment(){
     Payment payment = paymentRepo.findById(1);
     assertNotNull(payment);
  }

  @Test
  public void findByTypReturnsListOfPayment(){
    List<Payment> paymentList = paymentRepo.findByType("Cash");
    assertNotNull(paymentList);
    assertTrue(paymentList.size()>0);
  }

  @Test
  public void testSaveReturnsPayment(){
    Payment payment  = new Payment(2,new Date(),"Cash",130.00,1222);
    assertTrue(paymentRepo.save(payment) !=-1);
  }

}
