package com.allstate.payment.dao;

import com.allstate.payment.dto.Payment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaymentImplMongoTest {
    PaymentRepo paymentRepo = new PaymentImplMongo();

    @BeforeEach
    void setup(){
        paymentRepo.save(new Payment(1,new Date(),"Cash",1200.00,1));
    }

    @AfterEach
    public void tearDown(){
        ((PaymentImplMongo)paymentRepo).clearDB();
    }

    @Test
    public void isConnectedTest(){
        assertTrue(((PaymentImplMongo)paymentRepo).isConnected());
    }

    @Test
    public void testRowCount(){
        int count =  paymentRepo.rowCount();
        assertTrue(count>0);
    }

    @Test
    public void testFindByIdReturnsPayment(){
        Payment payment = paymentRepo.findById(1);
        assertNotNull(payment);
    }

    @Test
    public void testFindByTypeReturnsListOfPayment(){
        List<Payment> paymentList = paymentRepo.findByType("Cash");
        assertTrue(paymentList.size()>0);
    }

    @Test
    public void testSaveReturnsSuccess(){
        Payment payment = new Payment(5,new Date(),"Cash",1200.00,1);
        int saveCount  = paymentRepo.save(payment);
        assertTrue(saveCount!=-1);
    }
}
