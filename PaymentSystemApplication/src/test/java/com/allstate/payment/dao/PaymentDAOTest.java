package com.allstate.payment.dao;

import com.allstate.payment.dto.Payment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PaymentDAOTest {
    PaymentDAO paymentDAO;

    @BeforeEach
    void Setup() {
      paymentDAO = new PaymentDAO();
      paymentDAO.save(new Payment(1,new Date(),"Cash",120.00,1221));
    }

    @AfterEach
    void tearDown(){
        paymentDAO.clearDB();
    }

    @Test
    void testFindbyId(){
        assertEquals( 1,paymentDAO.findById(1).getId());
    }

    @Test
    void testFindByType(){
        assertEquals(1, paymentDAO.findByType("Cash").size());
    }

    @Test
    void testRowCount(){
        assertEquals( 1,paymentDAO.rowCount());
    }

    @Test
    void testSave(){
        Payment payment = new Payment();
        payment.setId(1);
        payment.setAmount(1300.00);
        payment.setPaymentDate(new Date());
        payment.setType("Cash");
        payment.setCustId(201);
        assertEquals(1,paymentDAO.save(payment));
    }
}