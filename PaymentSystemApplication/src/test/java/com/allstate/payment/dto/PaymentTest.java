package com.allstate.payment.dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentTest {
    Payment payment;

    @BeforeEach
    void setup(){
        payment = new Payment(1,new Date(),"Cash",1200.00,12201);
      ;
    }

    @Test
    void constructorTest(){
        assertEquals("Cash",payment.getType());
    }

    @Test
    void testToString() {
        assertEquals("[1] 1200.00 Cash 12201",payment.toString());
    }

    @Test
    void getId() {
        assertEquals(1,payment.getId());
    }

    @Test
    void setId() {
        payment.setId(2);
        assertEquals(2,payment.getId());
    }

    @Test
    void getCustomerId() {
        assertEquals(12201,payment.getCustId());
    }

    @Test
    void setCustomerId() {
        payment.setCustId(12034);
        assertEquals(12034,payment.getCustId());
    }

    @Test
    void getAmount() {
        assertEquals(1200.00,payment.getAmount());
    }

    @Test
    void setAmount() {
        payment.setAmount(1300.00);
        assertEquals(1300.00,payment.getAmount());
    }

    @AfterEach
    void tearDown() {
        payment = null;
    }
}
